Router.configure({
    layoutTemplate: 'layout',
    notFoundTemplate: 'notFound'
});

Router.route('/', {name: 'homeTemplate'});
Router.route('/about', {name: 'aboutTemplate'});
Router.route('/contactus', {name: 'contactUsTemplate'});
Router.route('/partners',{name: 'partnerWithUsTemplate'});
Router.route('/applycard',{name: 'applyForCardTemplate'});
